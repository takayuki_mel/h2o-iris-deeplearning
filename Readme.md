H2O-iris-prediction
======================
**iris-prediction** is the sample code How to use H2O POJO model on java project. 

Details
------
Details was wrote on our blog.  
[Data Analytics Park](http://bitbspdata.blogspot.jp/)  

Reference
----------
[H2O.ai](http://h2o.ai/)  
[H2O Document - POJO Quick Start](http://h2o-release.s3.amazonaws.com/h2o/rel-slater/8/docs-website/h2o-docs/index.html#POJO%20Quick%20Start)  
[UCI Machine Learning Repository - Iris Data Set](http://archive.ics.uci.edu/ml/datasets/Iris)
  