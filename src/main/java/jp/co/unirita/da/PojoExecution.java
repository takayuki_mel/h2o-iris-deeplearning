package jp.co.unirita.da;

import hex.genmodel.easy.EasyPredictModelWrapper;
import hex.genmodel.easy.RowData;
import hex.genmodel.easy.prediction.AutoEncoderModelPrediction;
import hex.genmodel.easy.prediction.MultinomialModelPrediction;

/**
 * Created by Kazuki Niimura on 2015/10/20.
 * H2O POJO Executor.
 */
public class PojoExecution {
    private static String modelClassName = "jp.co.unirita.da.IrisDeeplearning";

    public static void main(String[] args) throws Exception {
        // Create Model
        hex.genmodel.GenModel rawModel;
        rawModel = (hex.genmodel.GenModel) Class.forName(modelClassName).newInstance();
        EasyPredictModelWrapper model;
        model = new EasyPredictModelWrapper(rawModel);

        // Set Data
        RowData row = new RowData();
        for (String str : args) {
            String[] tokens = str.split("=");
            row.put(tokens[0], tokens[1]);
        }
        
        //  Prediction(Multinomial Model) And Print
        MultinomialModelPrediction p = model.predictMultinomial(row);
        System.out.println("Prediction Result: " + p.label);
    }
}
