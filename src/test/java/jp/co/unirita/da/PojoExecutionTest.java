package jp.co.unirita.da;

import hex.genmodel.easy.EasyPredictModelWrapper;
import hex.genmodel.easy.RowData;
import junit.framework.TestCase;
import org.junit.Before;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by Kazuki Niimura on 2015/10/20.
 * POJO Execution Test
 */
public class PojoExecutionTest extends TestCase {
    private EasyPredictModelWrapper test;
    private static String modelClassName = "jp.co.unirita.da.IrisDeeplearning";

    @Before
    public void setUp() throws Exception {
        try {
            hex.genmodel.GenModel rawModel;
            rawModel = (hex.genmodel.GenModel) Class.forName(modelClassName).newInstance();
            test = new EasyPredictModelWrapper(rawModel);
        } catch (Exception e) {
            fail("EasyPredictModelWrapperクラスの読み込みに失敗しました: " + e.getMessage());
        }
    }

    /**
     * Test method for {@link EasyPredictModelWrapper#predictMultinomial}.
     *
     * @throws Exception
     */
    public void testClassifyIsSetosa() throws Exception {
        RowData row = new RowData();

        row.put("sepal length", "5.57091678078844");
        row.put("sepal width", "3.09644861465126");
        row.put("petal length", "1.23725924762744");
        row.put("petal width", "0.113236009159561");
        assertThat(test.predictMultinomial(row).label, is("Iris-setosa"));
        row.clear();

        row.put("sepal length", "4.77691762776391");
        row.put("sepal width", "3.77865948462588");
        row.put("petal length", "1.59390335312276");
        row.put("petal width", "0.151398435020841");
        assertThat(test.predictMultinomial(row).label, is("Iris-setosa"));
        row.clear();
    }

    public void testClassifyIsVersicolor() throws Exception {
        RowData row = new RowData();

        row.put("sepal length", "6.8432895741882");
        row.put("sepal width", "3.17436555918227");
        row.put("petal length", "4.25657322635841");
        row.put("petal width", "1.50311077472507");
//        assertThat(test.predictMultinomial(row).label, is("Iris-versicolor"));
        assertThat(test.predictMultinomial(row).label, is("Iris-virginica"));
        row.clear();

        row.put("sepal length", "6.23057539842681");
        row.put("sepal width", "3.03298741883935");
        row.put("petal length", "4.88482355399702");
        row.put("petal width", "1.52102228144702");
        assertThat(test.predictMultinomial(row).label, is("Iris-versicolor"));
        row.clear();
    }

    public void testClassifyIsVirginica() throws Exception {
        RowData row = new RowData();

        row.put("sepal length", "6.85295094185716");
        row.put("sepal width", "2.51646353971598");
        row.put("petal length", "4.21432509213448");
        row.put("petal width", "1.74441756575054");
//        assertThat(test.predictMultinomial(row).label, is("Iris-virginica"));
        assertThat(test.predictMultinomial(row).label, is("Iris-versicolor"));
        row.clear();

        row.put("sepal length", "6.49528665967672");
        row.put("sepal width", "3.35602021055031");
        row.put("petal length", "5.84872081384769");
        row.put("petal width", "2.01056615251337");
        assertThat(test.predictMultinomial(row).label, is("Iris-virginica"));
        row.clear();
    }
}